import React, { Component } from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import 'jstree/dist/jstree.min';
import 'jstree/dist/themes/default/style.css';

class TreeView extends Component {

  static propTypes = {
    treeData: PropTypes.object.isRequired,
    events: PropTypes.array,
    onChange: PropTypes.func
  };

  static defaultProps = {
    onChange: () => false,
    events: () => []
  };

  shouldComponentUpdate(nextProps) {
    return JSON.stringify(nextProps.treeData) !== JSON.stringify(this.props.treeData);
  }

  componentDidMount() {
    const { treeData, events } = this.props;

    if (treeData) {
      $(this.treeContainer).jstree(treeData);
      $(this.treeContainer).on(events.join(' '), (e, data) => {
        this.props.onChange(e, data);
      });
    }
  }

  componentDidUpdate() {
    const { treeData } = this.props;
    if (treeData) {
      const settings = $(this.treeContainer).jstree(true).settings;
      settings.core = { ...settings.core, ...treeData.core };
      settings.plugins = [
        ...new Set(settings.plugins.concat(treeData.plugins)),
      ];

      $(this.treeContainer).jstree(true).settings = settings;
      $(this.treeContainer).jstree(true).refresh(true);
    }
  }

  render() {
    return (
      <div ref={div => this.treeContainer = div} />
    );
  }
}

export default TreeView;
